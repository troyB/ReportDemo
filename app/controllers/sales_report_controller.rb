class SalesReportController < ApplicationController

  def index
    if request.get?
    @sales=Sale.all
    else
      start_date=params[:start_date]
      end_date=params[:end_date]
      unless start_date.nil? || end_date.nil?
        @sales=Report.sales_by_product_in_range(start_date, end_date)
      end

    end
  end
end
