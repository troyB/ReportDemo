class Report
  def self.sales_by_product_in_range(start_date,end_date)
    Sale.where("date_of_sale >= '#{start_date}' and date_of_sale <= '#{end_date}'")
  end

  def self.sales_in_range_arl(start_date,end_date)
    sales=Sale.arel_table
    Sale.where(sales[:date_of_sale].between(start_date..end_date))
  end
end