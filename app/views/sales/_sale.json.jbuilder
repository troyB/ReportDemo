json.extract! sale, :id, :date_of_sale, :quantity, :product_id, :created_at, :updated_at
json.url sale_url(sale, format: :json)
