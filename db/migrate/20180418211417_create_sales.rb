class CreateSales < ActiveRecord::Migration[5.1]
  def change
    create_table :sales do |t|
      t.date :date_of_sale
      t.integer :quantity
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
